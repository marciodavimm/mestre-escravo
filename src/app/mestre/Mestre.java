package app.mestre;

import app.threads.ClienteThread;

import javax.swing.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Logger;

/**
 * Created by marcio on 30/08/14.
 */
public class Mestre {
    private final static int DISCOVERY_PORT = 8899;
    private final static int DISCOVERY_MAX_CLIENTS = 1;
    private final static int HTTP_PORT = 8877;
    private final static String DISCOVERY_REQUEST_MESSAGE = "SERVER_DISCOVERY_MESSAGE_REQUEST";
    private final static String DISCOVERY_RESPONSE_MESSAGE = "SERVER_DISCOVERY_MESSAGE_RESPONSE";
    private final static Logger logger = Logger.getLogger(Mestre.class.toString());
    private static ArrayList<InetAddress> listaIPs = new ArrayList();

    // endereco de broadcast (ifconfig)
//    private final static String DISCOVERY_IP = "192.168.1.255";
    // wifi santiago
//    private final static String DISCOVERY_IP = "192.168.43.255";
//    UNIRN wifi
//    private final static String DISCOVERY_IP = "177.154.114.127";
    //    UNIRN, ATRIO wifi
//    private final static String DISCOVERY_IP = "10.12.17.255";



    public static void main(String []args){
        try{
            // pega porta aleatoria
            Mestre c = new Mestre();
            DatagramSocket socket = new DatagramSocket();
            socket.setBroadcast(true);

            while(listaIPs.size() < DISCOVERY_MAX_CLIENTS) {
                c.exec(socket);
            }
            socket.close();

            System.out.println("\n\nRESULTADO:");
            for(InetAddress iaddr : listaIPs){
                System.out.println("host: " + iaddr.getHostAddress() );
            }

            System.out.println("\n\nCONECTANDO COM ESCRAVOS DESCOBERTOS...");
            String resultado = c.conectarEscravos();
//            System.out.println("\n\nRESULTADO DAS CONEXÕES:\n" + resultado);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void exec(DatagramSocket socket){
        try{
//            DatagramPacket sendPacket;

            byte[] sendData = DISCOVERY_REQUEST_MESSAGE.getBytes();

//            sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(DISCOVERY_IP), DISCOVERY_PORT);
//            socket.send(sendPacket);

            Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
            while(interfaces.hasMoreElements()){
                NetworkInterface ni = (NetworkInterface) interfaces.nextElement();
                if(ni.isLoopback() || !ni.isUp()){
                    continue; // pula interfaces inativas e de loopback
                }

                for( InterfaceAddress ia : ni.getInterfaceAddresses()) {
                    InetAddress broadcast = ia.getBroadcast();
                    if(broadcast == null) continue;

                    try{
                        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, broadcast, DISCOVERY_PORT);
                        socket.send(sendPacket);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    logger.info(getClass().getName() + "\n>>> pacote de requisicao enviado para " + broadcast.getHostAddress() + ",\ninterface: " + ni.getDisplayName());
                }
            }



            // esperando resposta...

            byte[] buffer = new byte[DISCOVERY_RESPONSE_MESSAGE.length()];
            DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
            socket.receive(receivePacket);

            logger.info(getClass().getName() + "\n>>> pacote de resposta recebido de " + receivePacket.getAddress().getHostAddress() );

            String msg = new String( receivePacket.getData() ).trim();
            if(msg.equals(DISCOVERY_RESPONSE_MESSAGE)){
//                synchronized( listaIPs ) {
                    if( !listaIPs.contains(receivePacket.getAddress() ) ) listaIPs.add(receivePacket.getAddress());
                System.out.println("\n>>> Lista: (size:"+listaIPs.size()+")");
                for(InetAddress iaddr : listaIPs){
                    System.out.println("host: " + iaddr.getHostAddress());
                }
                System.out.println();
//                }
            }

        }
        catch (Exception e){
//            logger.severe(e.getMessage());
            e.printStackTrace();
        }
    }

    private String conectarEscravos(){
        String resultado = "";

        ClienteThread[] escravos = new ClienteThread[listaIPs.size()];

        String initStr = JOptionPane.showInputDialog("PRIMOS: informe limite inferior (inclusive)");
        String endStr = JOptionPane.showInputDialog("PRIMOS: informe limite superior (inclusive)");
        int inicio = Integer.parseInt(initStr);
        int fim = Integer.parseInt(endStr);


        int intervalo = (fim-inicio) / listaIPs.size();
        long tempoInicial  = System.currentTimeMillis();

        int i=0;
        for(InetAddress iaddr : listaIPs){
//            ClienteThread clienteThread = new ClienteThread(iaddr.getHostAddress(), HTTP_PORT , "PRIMO:"+inicio+":"+fim);
            ClienteThread clienteThread = new ClienteThread(iaddr.getHostAddress()
                    , HTTP_PORT
                    , "PRIMO:"+( inicio + (intervalo * i) )+":"+ ( inicio+ intervalo * (i+1)) );
            escravos[i] = clienteThread;
            clienteThread.start();
            i++;
        }

        try {
            for(int j=0 ; j<escravos.length ; j++){
                escravos[j].join();
                System.out.println( "RESULTADO PARCIAL:\nESCRAVO ("+escravos[j].getHost()+"):\n"+ escravos[j].getResposta() );
                resultado += escravos[j].getResposta();
            }


            long tempoFinal = System.currentTimeMillis();

            System.out.println("TEMPO TOTAL GASTO: "+(tempoFinal - tempoInicial)+" ms");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return resultado;
    }

}
