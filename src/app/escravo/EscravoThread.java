package app.escravo;

import app.threads.ServidorThread;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.logging.Logger;

/**
 * Created by marcio on 30/08/14.
 */
public class EscravoThread implements Runnable {
    private final static int DISCOVERY_PORT = 8899;
    private final static int HTTP_PORT = 8877;

    private final static String DISCOVERY_REQUEST_MESSAGE = "SERVER_DISCOVERY_MESSAGE_REQUEST";
    private final static String DISCOVERY_RESPONSE_MESSAGE = "SERVER_DISCOVERY_MESSAGE_RESPONSE";
    private final static Logger logger = Logger.getLogger(EscravoThread.class.toString());
    private static EscravoThread instance = new EscravoThread();
    private DatagramSocket socket;

//    endereço de broadcast...
//    private final static String DISCOVERY_IP = "255.255.255.255";
//    UFRN, wifi UFRN
//    private final static String DISCOVERY_IP = "10.5.31.255";
//    UNIRN, LABREDES wifi
//    private final static String DISCOVERY_IP = "177.154.114.127";
    //    UNIRN, ATRIO wifi
//    private final static String DISCOVERY_IP = "10.12.17.255";
//    Casa
//    private final static String DISCOVERY_IP = "192.168.1.255";

    public static EscravoThread getInstance() {
        return instance;
    }

    private EscravoThread() {
    }


    @Override
    public void run() {
        String hostRemoto = escutarDescoberta();

        if(hostRemoto != null ) {
            ServidorThread servidor = new ServidorThread(hostRemoto , HTTP_PORT);
            servidor.start();
        }
    }

    private String escutarDescoberta(){
        try{
            socket = new DatagramSocket(DISCOVERY_PORT);

            socket.setBroadcast(true);

//            while(true){
            System.out.println(getClass().getName() + "\n\n>>> pronto para receber pacotes...");

            byte []receiveBuffer = new byte[15000];
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            socket.receive(receivePacket);

            System.out.println(getClass().getName() + "\n\n>>> pacote de descoberta recebido de: " + receivePacket.getAddress().getHostAddress() + ", \nporta: " + receivePacket.getPort());
            System.out.println(getClass().getName() + "\n>>> dados do pacote: " + new String(receivePacket.getData()) );

            String msg = new String(receivePacket.getData()).trim();
            if(msg.equals(DISCOVERY_REQUEST_MESSAGE)){
                byte[] sendBuffer = DISCOVERY_RESPONSE_MESSAGE.getBytes();

                DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, receivePacket.getAddress(), receivePacket.getPort());
                DatagramPacket sendPacket2 = new DatagramPacket(sendBuffer, sendBuffer.length);
                socket.send(sendPacket);

                System.out.println(getClass().getName() + "\n>>> pacote de resposta enviado para: " + sendPacket.getAddress().getHostAddress() );
            }
//            }
            return receivePacket.getAddress().getHostAddress();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
}
