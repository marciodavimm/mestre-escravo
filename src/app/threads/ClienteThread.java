package app.threads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public class ClienteThread extends Thread {
    public final static String HTTP_VERSION = "HTTP/1.1";
//    public final static int HTTP_PORT = 8877;
    private final static Logger logger = Logger.getLogger(ClienteThread.class.toString());

    private String host;
    private int porta;
    private String uri;
    private Socket socket;
    private String resposta="";

    /**
     * Construtor do cliente
     * @param host host a conectar
     * @param porta porta a ser usada na conexao
     */
    public ClienteThread(String host, int porta, String uri){
        super();
        this.host = host;
        this.porta = porta;
        this.uri = uri;
        logger.info("\n>> ("+getName()+") iniciando cliente (host: "+this.host+", porta: "+this.porta+", uri: "+this.uri+")...");
    }

    public String getHost() {
        return host;
    }
    public int getPorta() {
        return porta;
    }
    public Socket getSocket() {
        return socket;
    }
    public String getResposta() {
        return resposta;
    }


    @Override
    public void run() {
        try {
            this.resposta = enviarRequisicao(uri);
//            System.out.println(resposta);
//            logger.info(resposta);
        }
        catch (Exception e){
            logger.severe(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Cria um socket, com host e porta passados ao Construtor, faz uma requisição HTTP e
     * devolve a resposta como String.
     * @param uri a uri a ser usada na requisicao
     * @return resposta da requisição
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     */
    public String enviarRequisicao(String uri) throws UnknownHostException, IOException {
        try{
            this.socket = new Socket(host, porta);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            StringBuffer buffer = new StringBuffer();
            boolean loop = true;

            logger.info(">> enviando msg...");

            // monta e envia a requisição.
            out.println("GET " + uri + " " + HTTP_VERSION);
            out.println("Host: " + this.host);
            out.println("Connection: Close");
            out.println();

            logger.info(">> aguardando resposta...");
            while(loop){
                if(in.ready()){
                    int i=0;
                    while( (i = in.read()) != -1 ) buffer.append( (char)i );
                    loop=false;
                }
            }
            String resposta = buffer.toString();
//            logger.info("\n"+resposta);
            return resposta;
        }
        finally{
            if(socket != null) socket.close();
        }
    }

}
