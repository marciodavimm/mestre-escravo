package app.threads;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServidorThread extends Thread {
	public final static String HTTP_VERSION = "HTTP/1.1";
    private final static int HTTP_PORT = 8877;
	private final static Logger logger = Logger.getLogger(ServidorThread.class.toString());

	private String host="";
	private int porta;
	private ServerSocket serverSocket=null;

	/**
	 * Construtor que recebe um ServerSocket e recupera host e porta a partir dele.
	 * @param ss
	 */
	public ServidorThread(ServerSocket ss){
		this.serverSocket = ss;
		this.host = serverSocket.getInetAddress().getHostAddress();
		this.porta = serverSocket.getLocalPort();
		logger.info("\n\n>> (Construtor1) conexao iniciada com o servicor: " + this.host + ":" + this.porta);
	}

	/**
	 * Construtor que recebe host e porta, e cria um ServerSocket a partir destes.
	 * @param host
	 * @param porta
	 * @throws java.net.UnknownHostException
	 */
	public ServidorThread(String host, int porta){
		this.host = host;
		this.porta = porta;
		try{
			this.serverSocket = new ServerSocket( this.porta, 1, InetAddress.getByName(this.host) );			
			logger.info("\n\n>> (Construtor2) conexao iniciada com o servicor: " + this.host + ":" + this.porta);			
		}
		catch (IOException e){			
			logger.severe("\n\n>> ####### (Construtor2) erro ao iniciar servidor: " + this.host + ":" + this.porta);
			return;
		}
	}
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public ServerSocket getServerSocket() {
		return serverSocket;
	}
	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}
	public int getPorta() {
		return porta;
	}
	public void setPorta(int porta) {
		this.porta = porta;
	}
	
	
	@Override
    public void run(){
//		while(true){
			logger.info("\n\n>> Aguardando conexões...");
			Socket socket = null;
			InputStream input = null;
			OutputStream output = null;
			
			try{
				socket = serverSocket.accept();
				input = socket.getInputStream();
				output = socket.getOutputStream();
				
				// lê a primeira linha da requisição.
				String requisicaoStr = convertStreamToString(input);
				logger.info("\n\n>> Conexao aceita.\n>> Conteudo:\n" + requisicaoStr);
				String requisicaoUri = parse(requisicaoStr);

                String resultadoPrimos = montarRespostaPrimos(requisicaoUri);

                String respostaStr = criarResposta(requisicaoUri , resultadoPrimos);
				output.write(respostaStr.getBytes());
                logger.info("\n>> Resposta enviada.");

				socket.close();
			}
			catch (Exception e){
				logger.log(Level.SEVERE, "\n\n####### Erro ao executar servidor..\n\n" + e);
//				continue;
			}
//		}
		
	}

    private String montarRespostaPrimos(String requisicaoUri) {
        String resultado = "";

        // formato do pedido EX: "PRIMO:1:100"
        String[] valores = requisicaoUri.split(":");
        if(valores.length >=3){
            int inicio = Integer.parseInt(valores[1]);
            int fim = Integer.parseInt(valores[2]);

            long tempoInicio = System.currentTimeMillis();
            int totalPrimos = calcularPrimos(inicio, fim);
            long tempoFim = System.currentTimeMillis();

            resultado +="TOTAL DE PRIMOS: " + totalPrimos + "\n";
            resultado += "TEMPO GASTO: " +  (tempoFim - tempoInicio) + " ms\n";
        }
        else{ resultado += "PEDIDO MAL FORMATADO(USAR PADRAO 'PRIMO:INICIO:FIM')\n";
        }
        return resultado;
    }

    private int calcularPrimos(int inicio, int fim) {

        int cont = 0;
        for (int i = inicio; i <= fim; i++) {
            if(isPrimo(i)) cont++;
        }

        return cont;
    }

    public static boolean isPrimo(int x){
        for(int i=2 ; i<=(int)Math.sqrt(x) ; i++){
            if(x%i == 0 )return false;
        }
        return true;
    }

    private String convertStreamToString(InputStream is) {

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[2048];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is));
				int i = reader.read(buffer);
				writer.write(buffer, 0, i);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "\n\n####### Erro ao converter stream para string", e);
				return "";
			}
			return writer.toString();
		} else {
			return "";
		}
	}

    private String criarResposta(String uri, String resultadoPrimos){
        StringBuilder sb = new StringBuilder();
        // Cria primeira linha do status code, no caso sempre 200 OK
        sb.append("HTTP/1.1 200 OK").append("\r\n");

        // Cria os cabeçalhos
        sb.append("Test Server - URI: " + uri )
                .append("\r\n");
        sb.append("Content-Type: text/html; charset=UTF-8").append("\r\n");
        sb.append("Connection: Close").append("\r\n");
        sb.append("\r\n");

        // Agora vem o corpo em HTML
        sb.append("CORPO DA RESPOSTA: (" + uri+ ")\nRESULTADO DO PEDIDO:\n" + resultadoPrimos);

        return sb.toString();
    }

    /**
     * Realiza o parse de METODO (GET), URI (caminho) e PROTOCOLO ('HTTP/1.1')
     *
     * @param input
     * @return
     * @throws IOException
     */
    public String parse(String input) throws IOException {
        BufferedReader br = new BufferedReader(new StringReader(input));
        String line = null;
        int lineNumber = 0;
        while ((line = br.readLine()) != null) {
//            System.out.println(lineNumber + " " + line);
            if (lineNumber == 0) {
                String[] values = line.split(" ");
                if (values.length == 3) {
//                    this.method = values[0];
                    return values[1];
//                    this.protocol = values[2];
                }// TODO Tratar erro
            } else {
                // TODO Recuperar os cabeçalhos e corpo
            }
            lineNumber++;

        }
        return null;
    }

//	public static void main(String[] args) {
//		ServidorThread server;
//		server = new ServidorThread("localhost", 8082);
//		server.serve();
//	}
	
}
